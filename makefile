.DEFAULT_GOAL := all

all:

clean:
	cd examples/python; make clean
	@echo
	cd examples/javascript; make clean
	@echo
	cd examples/java; make clean

config:
	git config -l

dockerp:
	docker run -it -v $(PWD):/usr/cs373 -w /usr/cs373 gpdowning/python

dockern:
	docker run -it -v $(PWD):/usr/cs373 -w /usr/cs373 gpdowning/node

dockerj:
	docker run -it -v $(PWD):/usr/cs373 -w /usr/cs373 gpdowning/java

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs373.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

run:
	cd examples/python; make run
	@echo
	cd examples/javascript; make run
	@echo
	cd examples/java; make run

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Docker.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.py"                   \
    --include "Assertions.py"              \
    --include "UnitTests1.py"              \
    --include "UnitTests2.py"              \
    --include "UnitTests3.py"              \
    --include "Coverage1.py"               \
    --include "Coverage2.py"               \
    --include "Coverage3.py"               \
    --include "IsPrime.py"                 \
    --include "Exceptions.py"              \
    --include "Types.py"                   \
    --include "Operators.py"               \
    --include "Factorial.py"               \
    --include "FactorialT.py"              \
    --include "Reduce.py"                  \
    --include "ReduceT.py"                 \
    --include "RangeIterator.py"           \
    --include "RangeIteratorT.py"          \
    --include "Yield.py"                   \
    --include "Range.py"                   \
    --include "RangeT.py"                  \
    --include "Iteration.py"               \
    --include "Comprehensions.py"          \
    --include "Map.py"                     \
    --exclude "*"                          \
    ../../examples/python/ examples/python/
	@rsync -r -t -u -v --delete            \
    --include "Docker.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.js"                   \
    --include "Assertions.js"              \
    --include "UnitTests1.js"              \
    --include "UnitTests2.js"              \
    --include "UnitTests3.js"              \
    --include "Coverage1.js"               \
    --include "Coverage2.js"               \
    --include "Coverage3.js"               \
    --include "Exceptions.js"              \
    --include "Types.js"                   \
    --include "Operators.js"               \
    --include "Factorial.js"               \
    --include "FactorialT.js"              \
    --include "Reduce.js"                  \
    --include "ReduceT.js"                 \
    --include "RangeIterator.js"           \
    --include "RangeIteratorT.js"          \
    --include "Yield.js"                   \
    --include "Range.js"                   \
    --include "RangeT.js"                  \
    --include "Iteration.js"               \
    --include "Map.js"                     \
    --exclude "*"                          \
    ../../examples/javascript/ examples/javascript/
	@rsync -r -t -u -v --delete            \
    --include "Docker.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.java"                 \
    --include "Assertions.java"            \
    --include "Exceptions.java"            \
    --include "Types.java"                 \
    --include "Operators.java"             \
    --exclude "*"                          \
    ../../examples/java/ examples/java/
	@rsync -r -t -u -v --delete            \
    --include "Collatz.py"                 \
    --include "RunCollatz.in"              \
    --include "RunCollatz.out"             \
    --include "RunCollatz.py"              \
    --include "TestCollatz.py"             \
    --exclude "*"                          \
    ../../projects/python/collatz/ projects/collatz/

versions:
	cd examples/python; make versions
	@echo
	cd examples/javascript; make versions
	@echo
	cd examples/java; make versions
