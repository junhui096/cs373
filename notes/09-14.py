# -----------
# Fri, 14 Sep
# -----------

"""
in Java you get a default .equals() from Object
in Python you get a default ==
in both cases, the default does an identity check
"""

a = [2, 3, 4]
print(type(a)) # list

a = [2]
print(type(a)) # list

a = (2, 3, 4)
print(type(a)) # tuple

a = (2)
print(type(a)) # int

a = (2,)
print(type(a)) # tuple

print(type(list)) # type
print(type(type)) # type

"""
in Java   there's a class named class
in Python there's a type  named type
"""

"""
Python's int is like Java's BigInteger
"""

"""
Python's str is like Java's String
immutable
"""

"""
list is like Java's ArrayList
list is heterogenous
list is mutable
list is indexable
list is ordered

insertion
    back:  amortized O(1)
    other: O(n)
removal:
    back:  O(1)
    other: O(n)
index: O(1)
"""

print([2, 3] == [3, 2]) # False

"""
tuple is heterogenous
tuple is not mutable
tuple is indexable
tuple is ordered
"""

print((2, 3) == (3, 2)) # False

"""
set is like Java's HashSet
elements must be unique
elements must be hashable
elements are immutable
mutables    are not hashable: list, set, dict
immutalbles are     hashable: str, tuple, frozenset
set is not ordered
set is not indexable
set is iterable
"""

print({2, 3} == {3, 2}) # True

"""
frozenset is an immutable set
"""

for v in a : # a must only be iterable
    ...

print(type({2, 3})) # set
print(type({}))     # dict

"""
indexable is stronger than iterable
"""

"""
container constructors have a default constructor
and have a constructor that takes an iterable
"""

"""
dict is like Java's HashMap
keys must be hashable
keys are immutable
mutables    are not hashable: list, set, dict
immutalbles are     hashable: str, tuple, frozenset
dict is not ordered
dict is not indexable
dict is iterable, you get the keys
"""
