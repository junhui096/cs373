# -----------
# Mon, 10 Sep
# -----------

"""
mcl(10, 100)
b = 10
e = 100
m = 51
mcl(10, 100) = mcl(51, 100)
10 -> 80
15 -> 60
50 -> 100
"""

"""
memoization
build a cache
list, dict

lazy cache
cache the values when input is read and value computed

eager cache
precompute and cache the values before any input is read
"""

"""
problem author
    1. run out of input
    2. specify the amount of input at the beginning
    3. specify a sentinel at the end
"""

"""
IsPrime
    run it, succeed
    fix the tests
    run it, fail
    fix the code
    run it, succeed
"""
