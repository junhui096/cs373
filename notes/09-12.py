# -----------
# Wed, 12 Sep
# -----------

"""
what should we use for user errors
not assertions
exceptions
"""

"""
let's pretend we don't have exceptions
"""

# use the return

def f (...) :
    ...
    if (<something wrong>)
        return <special value>
    ...

def g (...) :
    ...
    x = f(...)
    if (x == <special value>)
        <something wrong>
    ...

# use a global

h = 0

def f (...) :
    global h
    ...
    if (<something wrong>)
        h = <special value>
        return ...
    ...

def g (...) :
    global h
    ...
    h = 0
    x = f(...)
    if (h == <special value>)
        <something wrong>
    ...

# use a parameter, int doesn't work

def f (..., e2) :
    ...
    if (<something wrong>)
        e2 = <special value>
        return ...
    ...

def g (...) :
    ...
    e1 = 0
    x = f(..., e1)
    if (e1 == <special value>)
        <something wrong>
    ...

# Java
# use a parameter, int doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>)
        e2 = <special value>;
        return ...;
    ...}

int g (...) {
    ...
    int e1 = 0;
    int i  = f(..., e1);
    if (e1 == <special value>)
        <something wrong>;
    ...}

# Java
# use a parameter, Integer doesn't work

int f (..., Integer e2) :
    ...
    if (<something wrong>) {
        e2 = <special value>;
        return ...;}
    ...

int g (...) :
    ...
    Integer e1 = 0;            # boxing
    x = f(..., e1);
    if (e1 == <special value>) # unboxing
        <something wrong>;
    ...

# use a parameter, list works

def f (..., e2) :
    ...
    if (<something wrong>)
        e2[0] = <special value>
        return ...
    ...

def g (...) :
    ...
    e1 = [0]
    x = f(..., e1)
    if (e1[0] == <special value>)
        <something wrong>
    ...

# use an exception

def f (...) :
    ...
    if (<something wrong>)
        raise Mammal(<msg>)
    ...
    if (<something wrong>)
        raise Tiger(<msg>)

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except (Tiger as e)
        <something wrong>
    except (Mammal as e)
        <something wrong>
    else
        ...
    finally
        ...
    ...

# use an exception

def f (...) :
    ...
    if (<something wrong>)
        raise Foo(<msg>)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except (E as e)
        <something wrong>
    else
        ...
    finally
        ...
    ...

a = [2, 3, 4]
print(a)       # [2, 3, 4]
print(type(a)) # list

a = (2, 3, 4)
print(a)       # (2, 3, 4)
print(type(a)) # tuple

# Java

String s = "abc";
String t = "abc";
System.out.print(s == t);      # false
System.out.print(s.equals(t)); # true

# Python

a = [2, 3, 4]
b = [2, 3, 4]
print(a == b) # true
print(a is b) # false

# Java

T s = new T(...);
T t = new T(...);
System.out.print(s == t);      # false
System.out.print(s.equals(t)); # true, only if T implements equals()

"""
Java's ==        is analogus to Python's is
Java's .equals() is analgous to Python's ==
"""
