# -----------
# Wed,  5 Sep
# -----------

"""
assertions
unit testing
coverage
"""

"""
take a pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat until 1
"""

1
5 16 8 4 2 1

"""
cycle length of  1 is 1
cycle length of  5 is 6
cycle length of 10 is 7
"""

"""
// floor division: the result, int -> int, float -> float
/  true  division: the result is always a float
"""

"""
assertions are good for preconditions, post conditions, loop invariants

assertions are not good for testing, unittest is
assertions are not good for user errors
"""

# Java

A x = new A(...);
x.f(...);         # this -> x
A.g(...);         # static method, there is no this


A y = new A(...);
y.f(...);         # this -> y

"""
Python's self is analogous to Java's this
but has to be explicitly introduced as the first argument
of the method

Java   static methods don't have this
Python static methods don't have self
"""
