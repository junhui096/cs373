# -----------
# Wed, 29 Aug
# -----------

"""
ACM is 80 years old
ICPC for about 40 years

run an annual competitive programming contest for universities

120 regions in the world

our region is SCUSA, run out of LSU
65 teams from 25 schools from 3 states

our teams: 12 students: 4 teams of 3

2014: 2nd, Rice 1st, Morocco
2015: 2nd, our 4 teams were all in the top 7, Rice 1st, Thailand
2016: 1st, 2nd, 3rd, 4th: South Dakota
2017: 1st: Beijing
2018: ?: Portugal

the good
    great practice of algorithm implementation
    great prep for interviewing

the bad
    production of non-industry-level code
    very undiverse
"""

"""
getting to know somebody
name
contact info
when are they graduating? then what?
why are they taking the class? now?
what are their expectations of the class
"""

"""
google "cs373 final entry"
"""
